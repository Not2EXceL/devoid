package io.not2excel.devoid

import java.io.File

import io.not2excel.devoid.lib.util.classloading.ClassEnumerator
import org.json4s.DefaultFormats
import org.json4s.native.JsonMethods

import scala.io.Source

object Devoid {

    def main(args: Array[String]): Unit = {

        implicit val formats = DefaultFormats

        val stream = this.getClass.getResourceAsStream("/client.json")
        val clientJsonData = Source.fromInputStream(stream).getLines().mkString("\n")
        println(clientJsonData)
        val clientJson = JsonMethods.parse(clientJsonData)
        println(clientJson)

        val file = new File((clientJson \ "mc_jar").extract[String])
        ClassEnumerator.loadJarClasses(file)
//            .foreach(c => println(c.getName))

//        ClassEnumerator.internalClasses.foreach(c => {
//            if(c.getName.contains("io.not2excel.")) {
//                println(c.getName)
//            }
//        })
    }
}
